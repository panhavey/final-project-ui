import React, { Fragment } from "react";
import CardExample from "./components/CardExample";
import { BrowserRouter as Router } from "react-router-dom";
import CommerceNav from "./components/navbar/Navbar";
import Homepage from "./components/homepage/Homepage";
import { MDBInput } from "./mdbreact";

function App() {
  return (
    <div className="">
      <Router>
        <CommerceNav />
      </Router>
      <Homepage />
    </div>
  );
}

export default App;
