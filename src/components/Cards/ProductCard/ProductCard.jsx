import React from "react";
import "./productcard.css";
import {
  MDBBtn,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBCardTitle,
  MDBBox,
  MDBIcon,
  MDBTypography,
  MDBCardText,
  MDBTooltip,
  MDBCardFooter,
  MDBAnimation,
  MDBCol,
} from "../../../mdbreact";
import Truncate from "react-truncate";

const ProductCard = () => {
  let t =
    "Product name is asnasfaslfn alssaflanfa  ksaskfkaskfjak asjbfkjasbkfbasbfkjabsjkfbkajsbfk  asfasb";
  return (
    <div className="col">
      <MDBCard className="m-2" ecommerce narrow>
        <MDBCardImage
          top
          src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/img%20(5).jpg"
          waves
        />
        <a
          href="#"
          className="store-name font-weight-light white-text font-small"
        >
          <span>
            {" "}
            <MDBIcon icon="store" className="mr-2" />
            Store name
          </span>
        </a>

        <MDBCardBody className="mt-n1 m-n2">
          <MDBCardTitle className="teal-text">
            <Truncate lines={2}>Dress and Shoesss</Truncate>
          </MDBCardTitle>
          <MDBCardText>
            <MDBBox tag="p">
              <del>$500</del>
            </MDBBox>
            <MDBCardText className="d-flex justify-content-between">
              <MDBBox
                tag="span"
                className="mdb-color red white-text py-1 pl-2 pr-3 z-depth-1 "
                style={{ borderRadius: "0px 50px 50px 0px", marginLeft: -13 }}
              >
                50% OFF
              </MDBBox>
              <MDBBox
                tag="span"
                className="lead orange-text font-weight-bolder "
              >
                $250
              </MDBBox>
            </MDBCardText>
          </MDBCardText>
          <MDBCardFooter>
            <span
              className="float-left font-small text-muted p-1"
              style={{ marginLeft: "-1.25rem" }}
            >
              <MDBIcon far icon="eye" className="mr-1" />
              100
            </span>
            <span className="float-right" style={{ marginRight: "-1.25rem" }}>
              <MDBTooltip placement="top" email>
                <MDBBtn
                  tag="a"
                  href="#"
                  target="_blank"
                  color="transparent"
                  size="lg"
                  className="p-1 m-0 mr-2 z-depth-0 cart"
                >
                  <MDBIcon icon="shopping-bag" />
                </MDBBtn>
                <div>Add to cart</div>
              </MDBTooltip>
              <MDBTooltip placement="top" email>
                <MDBBtn
                  tag="a"
                  color="transparent"
                  size="lg"
                  className="p-1 m-0 z-depth-0"
                >
                  <MDBIcon far icon="heart" />
                </MDBBtn>
                <div>Add to Wishlist</div>
              </MDBTooltip>
            </span>
          </MDBCardFooter>
        </MDBCardBody>
      </MDBCard>
    </div>
  );
};

export default ProductCard;
