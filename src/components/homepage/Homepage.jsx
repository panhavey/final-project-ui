import React, { Component } from "react";
import Popular from "./popular/Popular";
import Banner from "./banner/banner";
import NewArrival from "./newArrival/NewArrival";

class Homepage extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div>
        <Banner />
        <div className="container">
          <NewArrival />
          <Popular />
        </div>
      </div>
    );
  }
}

export default Homepage;
