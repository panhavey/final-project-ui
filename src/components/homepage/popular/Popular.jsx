import React from "react";
import CardExample from "../../CardExample";
import { MDBTypography } from "../../../mdbreact";

const Popular = () => {
  return (
    <div className="orange-text mt-4">
      <MDBTypography tag="h3" variant="h3-responsive">
        Popular Product
      </MDBTypography>
      <hr />
      <CardExample />
    </div>
  );
};

export default Popular;
