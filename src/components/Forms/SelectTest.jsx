import React, { Component } from "react";
import { MDBSelect,MDBSelectOptions,MDBSelectInput,MDBSelectOption } from "../../mdreact";

class SelectTest extends Component {
  state = {
    options: [
      {
        disabled: true,
        text: "team 1"
      },
      {
        text: "Option 1",
        value: "1"
      },
      {
        text: "Option 2",
        value: "2"
      },
      {
        disabled: true,
        text: "team 2"
      },
      {
        checked: true,
        text: "Option 3",
        value: "3"
      },
      {
        text: "Option 4",
        value: "4"
      }
    ]
  };

  render() {
    return (
      <div>
        <select className="browser-default custom-select">
          <option>Choose your option</option>
          <option value="M">Male</option>
          <option value="F">Female</option>
          <option value="o">Other</option>
        </select>
      </div>
    );
  }
}

export default SelectTest;