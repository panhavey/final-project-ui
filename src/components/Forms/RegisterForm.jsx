import React from "react";
import { MDBContainer, MDBRow, MDBCol, MDBCard, MDBCardBody, MDBInput,MDBSelect,MDBInputGroup, MDBBtn, MDBIcon } from '../../mdreact';

 const RegisterForm = () =>  {
    

  return (
    <MDBContainer>
      <MDBRow>
        <MDBCol md="4.5">
          <MDBCard>
            <div className="header pt-4" style={{backgroundColor:"#009F90",borderRadius:"4px"}}>
              <MDBRow className="d-flex justify-content-center mb-3">
                <h3 className="white-text mb-3 pt-3 font-weight-bold ">
                  Register
                </h3>
              </MDBRow>
             
            </div>
            <MDBCardBody className="col2" style={{paddingTop:"5px"}}>
            <div className="grey-text">
                  <MDBInput
                    label="Username"
                    icon="user"
                    group
                    type="text"
                    validate
                    error="wrong"
                    success="right"
                  /> 
            </div>
            <div className="grey-text">  
                <MDBInput 
                label="Email" 
                icon="envelope" 
                color="grey" 
                group 
                type="text" 
                validate 
                style={{marginTop:"3px"}}
                />
            </div>
            <div className="grey-text">
                <MDBInput
                label="Password"
                icon="lock "
                group
                type="password"
                validate
                containerClass="mb-0"
                />
            </div>
            <div className="grey-text">
                <MDBInput
                label="Phone number"
                icon="mobile"
                group
                type="text"
                validate
                containerClass="mb-0"
                />
            </div >
            <MDBRow>
            <div className="grey-text" style={{marginLeft:"30px",marginTop:"-30px"}}>
            <MDBInput
             hint="Age"
             group
             type="number"
             validate
             containerClass="col-6"
           
            />
        
            </div>
            <div className="col-5" >
           <select 
           className="browser-default custom-select" >
                <option> Gender</option>
                <option value="1">Male</option>
                <option value="2">Female</option>
                <option value="3">Other</option>
                </select>
            </div>
            </MDBRow> 
              <MDBCol lg="" className="White-text d-flex justify-content-center">
            
                  <div className="text-center ">
                    <MDBBtn
                      outline
                      rounded
                      type="button"
                      className="z-depth-1a font-weight-bold"
                      style={{width:"350px", height:"50px", marginBottom:"20px",}}>
                      Register
                    </MDBBtn>
                  </div>
                </MDBCol>
                <MDBCol className="grey-text text-center">By creating an account, You Agree to the DerPhsar.com</MDBCol>
                <MDBCol className="grey-text text-center">Free Membership Agreement and Privacy Policy</MDBCol>                
              
                <MDBCol className="text-center">
                Quick access with:
                </MDBCol>
              
                <MDBRow >
                    <MDBCol className="d-flex justify-content-center"> 
                    <div className="row my-3 d-flex justify-content-center">
                    <MDBBtn tag="a" size="lg" floating gradient="blue">
                    <MDBIcon fab icon="facebook-f" />
                    </MDBBtn>
                    <MDBBtn tag="a" size="lg" floating color="#ef5350 red lighten-1">
                    <MDBIcon fab icon="google" />
                    </MDBBtn>  
                    </div>
                    </MDBCol>
                </MDBRow>
            </MDBCardBody>
          </MDBCard>
        </MDBCol>
      </MDBRow>
    </MDBContainer>
  );
};



export default RegisterForm;